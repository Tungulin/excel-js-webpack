export function capitalize(EventName) {
    if (typeof EventName !== "string") {
        return ''
    }
    return EventName.charAt(0).toUpperCase() + EventName.slice(1)
}