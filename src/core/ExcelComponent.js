import { DomListener } from '@core/DomListener'

export class ExcelComponent extends DomListener {
    constructor($root, options = {}) {
        super($root, options.listeners)
        this.name = options.name
        this.emitter = options.emitter
        this.prepare()
    }

    prepare() {

    }

    $emit(event, ...args) {
        this.emitter.emit(event, ...args)
    }

    $on(event, fnc) {
        this.emitter.subscribe(event, fnc)
    }

    toHTML() {
        return ''
    }

    init() {
        this.initDOMListeners()
    }

    destroy() {
        this.removeDOMListeners()
    }
}