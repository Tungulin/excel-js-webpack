import { capitalize } from "@core/utils"

export class DomListener {
    constructor($root, listeners = []) {
        if (!$root) {
            throw new Error('Not $root in DomListener')
        }
        this.$root = $root
        this.listeners = listeners
    }

    initDOMListeners() {
        this.listeners.forEach(listener => {
            const method = getMethodName(listener)
            if (!this[method]) {
                throw new Error(
                    `Method ${method} in ${this.name ||'NoN'}  is defined.`
                )
            }
            this[method] = this[method].bind(this)
            this.$root.on(listener, this[method]) //Function addEventListener

        })
    }

    removeDOMListeners() {
        this.listeners.forEach(listener =>{
            const method = getMethodName(listener)
            this.$root.off(listener,this[method])
        })
    }
}

function getMethodName(EventName) {
    return 'on' + capitalize(EventName)
}