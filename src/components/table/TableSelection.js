export class TableSelect {
    constructor() {
        this.group = []
        this.current
    }

    select($el) {
        this.remove()
        this.group.push($el) 
        this.current = $el
        $el.focus().addClass('selected') 
    }

    remove(){
        this.group.forEach($el=> $el.removeClass('selected'))
        this.group = []
    }

    selectGroup() {
        /*Method for select more table*/
    }
}