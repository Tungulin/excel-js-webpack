import { ExcelComponent } from '@core/ExcelComponent'
import { createTable } from './tableTemplate'
import { $ } from '@core/dom'
import { shouldResize } from './tableFunctions'
import { resizeHandler } from './tableResize'
import { TableSelect } from './TableSelection'

export class Table extends ExcelComponent {
  static className = 'excel__table'

  constructor($root, options) {
    super($root, {
      name: 'Table',
      listeners: ['mousedown', 'keydown', 'input'],
      ...options
    })
    this.$root = $root
    this.maxRows = 20
  }

  toHTML() {
    return createTable(this.maxRows)
  }

  prepare() {
    this.selection = new TableSelect()
  }

  init() {
    super.init()
    this.selectCell(this.$root.find('[data-id="0:0"]'))

    this.$on('formula:input', text => {
      this.selection.current.text(text)
    })

    this.$on('formula:done', () => {
      this.selection.current.focus()
    })
  }

  selectCell($cell) {
    this.selection.select($cell)
    this.$emit('table:select', $cell)
  }

  onMousedown(event) {
    if (shouldResize(event)) {
      resizeHandler(this.$root, event)
    } else if (event.target.dataset.id) {
      this.selection.select($(event.target))
    }
  }

  onKeydown(event) {
    const keys = ['ArrowRight', 'ArrowLeft', 'ArrowUp', 'ArrowDown', 'Tab', 'Enter'],
      { key } = event
    if (keys.includes(key) && !event.shiftKey) {
      event.preventDefault()
      const id = this.selection.current.id(true)
      const $next = this.$root.find(nextSelector(key, this.maxRows, id))
      this.selection.select($next)
      this.$emit('table:select', $next)
    }
  }

  onInput(event){
    this.$emit('table:input', $(event.target))
  }
}

function nextSelector(key, maxRows, { row, col }) {
  const MIN_VALUE = 0,
    MAX_VALUE_ROWS = maxRows - 1,
    MAX_VALUE_COLS = 25

  switch (key) {
    case 'Enter':
    case 'ArrowRight':
      col = col + 1 > MAX_VALUE_COLS ? MAX_VALUE_COLS : col + 1
      break
    case 'Tab':
    case 'ArrowDown':
      row = row + 1 > MAX_VALUE_ROWS ? MAX_VALUE_ROWS : row + 1
      break
    case 'ArrowLeft':
      col = col - 1 < MIN_VALUE ? MIN_VALUE : col - 1
      break
    case 'ArrowUp':
      row = row - 1 < MIN_VALUE ? MIN_VALUE : row - 1
      break
  }
  return `[data-id ="${row}:${col}"]`
}




//259 msScripting
//1914 msRendering

//97 msScripting
//1482 msRendering