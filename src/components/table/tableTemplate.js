const CODES = {
  A: 65,
  Z: 90
}

let rowsCOUNT = 0;

/* data-id = "${colI}:${index}">  */

function createCol(context = '', index, isContentEdit, colI = '') {
  return `
    <div class = "column"
    contenteditable = "${isContentEdit}" 
    data-type = "${isContentEdit?'resizable':''}" 
    data-col = ${index}
    ${colI === ''?'':`data-id = "${colI}:${index}"`}>
      ${context}
    ${context ? '<div class = "col-resize" data-resize = "col"></div>' : ''}
    </div>`
}

function createRow(content, classRow = '', dataRowInfo = '') {
  const resize = !(dataRowInfo === rowsCOUNT) ?
    '<div class = "row-resize" data-resize = "row"></div>' : ''

  return `<div class="row" data-type = "resizable">
    <div class="row-info important">
    ${dataRowInfo}
    ${resize}
    </div>
    <div class="row-data ${classRow}">
        ${content}
    </div>
  </div>`
}

function toChar(index) {
  return String.fromCharCode(CODES.A + index)
}

function toNumber(index) {
  return 1 + index
}

export function createTable(rowsCount = 20) {
  const countCODES = CODES.Z - CODES.A + 1
  const rows = []
  rowsCOUNT = rowsCount
  const rowsInfo = new Array(rowsCount)
    .fill('')
    .map((el, index) => toNumber(index))


  const cols = new Array(countCODES)
    .fill('')
    .map((el, index) => toChar(index))
    .map((el, index) => createCol(el, index, 'false'))
    .join('')

  rows.push(createRow(cols, 'important'))

  for (let i = 0; i < rowsCount; i++) {
    const columns = new Array(countCODES)
      .fill('')
      .map((el, index) => createCol('', index, 'true', i))
      .join('')

    rows.push(createRow(columns, '', rowsInfo[i]))
  }

  return rows.join('')
}