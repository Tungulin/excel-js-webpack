import {$} from '@core/dom'

export function resizeHandler($root,event) {
    const $resizer = $(event.target),
    $parent = $resizer.closest('[data-type = "resizable"]'),
    coords = $parent.getCoords(),
    cells = $root.queryAll(`[data-col ="${$parent.data.col}"]`),
    type = $resizer.data.resize
  let value
  if (type === 'col') $resizer.css({ opacity: 1, bottom: '-600px' })
  else $resizer.css({ opacity: 1, width: $parent.$el.offsetWidth + 'px'}) 

  document.onmousemove = e => {
    if (type === 'col') {
      const delta = e.pageX - coords.right
      value = coords.width + delta
      $resizer.css({ right: -delta + 'px' })
    } else {
      const delta = e.pageY - coords.bottom
      $resizer.css({ bottom: -delta + 'px' })
      value = coords.height + delta
    }
  }

  document.onmouseup = () => {
    document.onmousemove = null
    document.onmouseup = null

    if (type == 'col') {
      $parent.css({ width: value + 'px' })
      cells.forEach(el => el.style.width = value + 'px')
      $resizer.css({ opacity: 0, right: '-2px',bottom:0 })
    } else {
      $parent.css({ height: value + 'px' })
      $resizer.css({ opacity: 0, bottom: '-2px', width:'auto' })
    }

  }
}