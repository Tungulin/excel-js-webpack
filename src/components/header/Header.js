import { ExcelComponent } from '@core/ExcelComponent'

export class Header extends ExcelComponent {
  static className = 'excel__header'

  constructor($root, options) {
    super($root, {
      name: 'Header',
      listeners: [],
      ...options
    })
  }

  toHTML() {
    return `
        <div class="left-content">
        <img />
        <input class="input" type="text" value="New table" />
      </div>
      <div class="right-content">
        <span class="material-icons">
          delete_forever
        </span>
        <span class="material-icons">
          arrow_forward_ios
        </span>
      </div>
        `
  }
}