import { $ } from '@core/dom'
import { Emitter } from '@core/Emitter'

export class Excel {
    constructor(selector, options) {
        this.$el = $(selector)
        this.componets = options.componets || []
        this.emitter = new Emitter()
    }

    getRoot() {
        const $root = $.create('div', 'excel'),
            options = { emitter: this.emitter }

        this.componets = this.componets.map(Component => {
            const $el = $.create('div', Component.className)
            const component = new Component($el, options)
            $el.html(component.toHTML())
            $root.append($el)
            return (component)
        });

        return $root
    }

    render() {
        this.$el.append(this.getRoot())
        this.componets.forEach(component => component.init());
    }
}